from django.urls import path
from core.erp.views import CategoriaListView, CategoriaCreateView,CategoriaUpdateView, MarcaListView, \
    CreateMarcaView, ProductoListView, ProductoCreateView, CategoriaDeleteView, MarcaUpdateView, MarcaDeleteView, \
    ProductoUpdateView, ProductoDeleteView

app_name = 'erp'
urlpatterns = [
    path('categoria-list/', CategoriaListView.as_view(), name="categoria-list"),
    path('categoria-create/', CategoriaCreateView.as_view(), name="categoria-create"),
    path('categoria-update/<pk>/', CategoriaUpdateView.as_view(), name="categoria-update"),
    path('categoria-delete/<pk>/', CategoriaDeleteView.as_view(), name="categoria-delete" ),
    path('marca-list/', MarcaListView.as_view(), name="maraca-list"),
    path('marca-create/', CreateMarcaView.as_view(), name="marca-create"),
    path('marca-update/<pk>/', MarcaUpdateView.as_view(), name="marca-update"),
    path('marca-delete/<pk>/', MarcaDeleteView.as_view(), name="marca-delete"),
    path('producto-list', ProductoListView.as_view(), name="producto-list"),
    path('producto-create/', ProductoCreateView.as_view(), name="producto-create"),
    path('producto-update/<pk>/', ProductoUpdateView.as_view(), name="producto-update"),
    path('producto-delte/<pk>/', ProductoDeleteView.as_view(), name="producto-delete")

]
from django.forms import ModelForm, TextInput, Textarea, NumberInput, Select
from core.erp.models import Category, Brand, Product

class CategoriaForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    class Meta:
        model = Category
        fields = '__all__'
        widgets = {
            'name' : TextInput(
                attrs= {
                    'placeholder': 'Nombre'
                }
            ),
            'desc' : Textarea(
                attrs= {
                    'placeholder' : 'Descripcion',
                    'rows' : 4,
                    'cols': 5
                }
            )
        }

class BrandForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class']= 'form-control'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    class Meta:
        model = Brand
        fields = '__all__'
        widgets = {
            'name' : TextInput(
                attrs={
                    'placeholder' : 'Nombre de la Marca'
                }
            ),
            'desc' : Textarea(
                attrs={
                    'placeholder': 'Descripción',
                    'rows': 4,
                    'cols': 5,
                }
            )
        }

class ProductoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class']= 'form-control'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    class Meta:
        model = Product
        fields = '__all__'
        exclude = ['image']
        widgets = {
            'name' : TextInput(
                attrs={
                    'placeholder' : 'Nombre del Producto'
                }
            ),
            'pvp' : NumberInput(
                attrs={
                    'placeholder': 'Precio',
                    'step':0.25

                }
            ),
            'cate' : Select(
                attrs={
                    'placeholder' : 'Categoria'
                }
            ),
            'marca': Select(
                attrs={
                    'placeholder': 'Marca'
                }
            ),

        }

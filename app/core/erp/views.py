from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, ListView, CreateView, UpdateView,DeleteView
from core.erp.models import Category, Brand, Product
from django.utils.decorators import method_decorator
from core.erp.forms import CategoriaForm, BrandForm, ProductoForm
# Create your views here.

#Categorias
class CategoriaListView(ListView):
    model = Category
    template_name = 'categoria/list.html'
    context_object_name = 'categoria'

    #@method_decorator(login_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        #print(request)
        return super().dispatch(request, *args, **kwargs)

    # def post(self,request, *args, **kwargs):
    #     data = {}
    #     #print(request.POST)
    #     try:
    #         data = Category.objects.get(pk=request.POST['id']).toJSON()
    #     except Exception as e:
    #         data['error'] = str(e)
    #     return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Lista de  Categorias"
        context['create_url'] = reverse_lazy('erp:categoria-create')
        context['list_url'] = reverse_lazy('erp:categoria-list')
        context['update_url'] = reverse_lazy('erp:categoria-update')
        context['entity'] = 'Categorias'
        return context

    def get_queryset(self):
        queryset = Category.objects.all()
        return queryset


class CategoriaCreateView(CreateView):
    model = Category
    form_class = CategoriaForm
    template_name = 'categoria/create.html'
    success_url = reverse_lazy('erp:categoria-list')

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Creacion de una Categoria"
        context['entity'] = 'Categorias'
        context['list_url'] = reverse_lazy('erp:categoria-list')
        context['acction'] = 'add'
        return context


class CategoriaUpdateView(UpdateView):
    model = Category
    template_name = 'categoria/update.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('erp:categoria-list')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'update':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Actualizar la Categoria"
        context['entity'] = 'Categorias'
        context['list_url'] = reverse_lazy('erp:categoria-list')
        context['acction'] = 'update'
        return context


class CategoriaDeleteView(DeleteView):
    model = Category
    template_name = 'categoria/delete.html'
    success_url = reverse_lazy('erp:categoria-list')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Eliminar la Categoria"
        context['entity'] = 'Categorias'
        context['list_url'] = reverse_lazy('erp:categoria-list')
        return context


#Marcas ----------------------------------------------------------------------

class MarcaListView(ListView):
    model = Brand
    template_name = 'marca/list.html'
    context_object_name = 'marca'

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args,**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Lista de Marcas'
        context['create_url'] = reverse_lazy('erp:marca-create')
        context['list_url'] = reverse_lazy('erp:maraca-list')
        context['entity'] = 'Marcas'
        return context

    def get_queryset(self):
        queryset = Brand.objects.all()
        return queryset

class CreateMarcaView(CreateView):
    model = Brand
    template_name = 'marca/create.html'
    form_class = BrandForm
    success_url = reverse_lazy('erp:maraca-list')

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Creacion de una Marca"
        context['entity'] = 'Marcas'
        context['list_url'] = reverse_lazy('erp:maraca-list')
        context['acction'] = 'add'
        return context

class MarcaUpdateView(UpdateView):
    model = Brand
    template_name = 'marca/update.html'
    form_class = BrandForm
    success_url = reverse_lazy('erp:maraca-list')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'update':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Actualizar la Marca"
        context['entity'] = 'Marcas'
        context['list_url'] = reverse_lazy('erp:maraca-list')
        context['acction'] = 'update'
        return context

class MarcaDeleteView(DeleteView):
    model = Brand
    template_name = 'marca/delete.html'
    success_url = reverse_lazy('erp:maraca-list')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Eliminar la Marca"
        context['entity'] = 'Marcas'
        context['list_url'] = reverse_lazy('erp:maraca-list')
        return context


#Producto----------------------------------------------------------
class ProductoListView(ListView):
    model = Product
    template_name = 'producto/list.html'
    context_object_name = 'productos'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args,**kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Product.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Lista de Productos'
        context['create_url'] = reverse_lazy('erp:producto-create')
        context['list_url'] = reverse_lazy('erp:producto-list')
        context['entity'] = 'Productos'
        return context

    def get_queryset(self):
        queryset = Product.objects.all()
        return queryset

class ProductoCreateView(CreateView):
    model = Product
    template_name = 'producto/create.html'
    form_class = ProductoForm
    success_url = reverse_lazy('erp:producto-list')

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Creacion de un Producto"
        context['entity'] = 'Productos'
        context['list_url'] = reverse_lazy('erp:producto-list')
        context['acction'] = 'add'
        return context

class ProductoUpdateView(UpdateView):
    model = Product
    template_name = 'producto/update.html'
    form_class = ProductoForm
    success_url = reverse_lazy('erp:producto-list')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'update':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Actualizar el Producto"
        context['entity'] = 'Productos'
        context['list_url'] = reverse_lazy('erp:producto-list')
        context['acction'] = 'update'
        return context

class ProductoDeleteView(DeleteView):
    model = Product
    template_name = 'producto/delete.html'
    success_url = reverse_lazy('erp:producto-list')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self,request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Eliminar el Producto"
        context['entity'] = 'Productos'
        context['list_url'] = reverse_lazy('erp:producto-list')
        return context

from django.db import models
from django.forms import model_to_dict

from core.erp.models import Product
from datetime import datetime

# Create your models here.
class PaymentTerm(models.Model):

    TIPO_CLIENTE = 'C'
    TIPO_PROVEEDOR = 'P'

    PAGO_EFECTIVO = 'E'
    PAGO_TARJETA = 'T'
    PAGO_CHEQUE = 'C'
    PAGO_OTRO = 'O'

    TIPO_PAGO = (
        (PAGO_EFECTIVO, 'Efectivo'),
        (PAGO_TARJETA, 'Tarjeta'),
        (PAGO_CHEQUE, 'Cheque'),
        (PAGO_OTRO, 'Otro')
    )

    TIPOS = (
        (TIPO_CLIENTE, 'Cliente'),
        (TIPO_PROVEEDOR, 'Proveedor')
    )

    code = models.CharField(max_length=150,verbose_name='Codigo')
    name = models.CharField(max_length=150, verbose_name='Nombre')
    type = models.CharField(max_length=50, verbose_name='Tipo', choices=TIPOS, default=TIPO_CLIENTE)
    pay_type = models.CharField(max_length=50, verbose_name='Tipo Pago', choices=TIPO_PAGO, default=PAGO_EFECTIVO)
    starting_amount = models.DecimalField(default=0.00, max_digits=9,decimal_places=2, verbose_name='Monto Inicial')
    enable = models.BooleanField(default=True, verbose_name='Habilitado')
    desc = models.CharField(max_length=500, verbose_name='Descripción', null=True,blank=True)
    days = models.IntegerField(default=0, verbose_name='Días')
    amount = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Monto')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Termino Pago'
        verbose_name_plural = 'Terminos Pago'
        ordering = ['id']



class Client(models.Model):

    names = models.CharField(max_length=150, verbose_name='Nombres')
    surnames = models.CharField(max_length=150, verbose_name='Apellidos')
    nit = models.CharField(max_length=10, unique=True, verbose_name='Nit')
    address = models.CharField(max_length=150, null=True, blank=True, verbose_name='Dirección')
    phone = models.CharField(max_length=50, null=True, blank=True, verbose_name='Teléfono')
    term = models.ForeignKey(PaymentTerm, on_delete=models.CASCADE, verbose_name='Termino de Pago')
    balance = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.names

    def get_full_name(self):
        return '{} {} / {}'.format(self.names, self.surnames, self.nit)

    def toJSON(self):
        item = model_to_dict(self)
        item['full_name'] = self.get_full_name()
        return item

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['id']


class Sale(models.Model):
    cli = models.ForeignKey(Client, on_delete=models.CASCADE)
    term = models.ForeignKey(PaymentTerm, on_delete=models.CASCADE, null=True, blank=True)
    date_sale = models.DateField(default=datetime.now)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    balance = models.DecimalField(default=0.00,max_digits=9,decimal_places=2)

    def __str__(self):
        return self.cli.names

    def toJSON(self):
        item = model_to_dict(self)
        item['cli'] = self.cli.toJSON()
        item['subtotal'] = format(self.subtotal, '.2f')
        item['iva'] = format(self.iva, '.2f')
        item['total'] = format(self.total, '.2f')
        item['balance'] = format(self.balance, '.2f')
        item['date_sale'] = self.date_sale.strftime('%Y-%m-%d')
        item['det'] = [i.toJSON() for i in self.detsale_set.all()]
        return item

    def delete(self, using=None, keep_parents=False):
        for det in self.detsale_set.all():
            det.prod.stock += det.cant
            det.prod.save()
        super(Sale, self).delete()

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'
        ordering = ['id']


class DetSale(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.prod.name

    def toJSON(self):
        item = model_to_dict(self, exclude=['sale'])
        item['prod'] = self.prod.toJSON()
        item['price'] = format(self.price, '.2f')
        item['subtotal'] = format(self.subtotal, '.2f')
        return item

    class Meta:
        verbose_name = 'Detalle de Venta'
        verbose_name_plural = 'Detalle de Ventas'
        ordering = ['id']


from django.forms import ModelForm, TextInput, Textarea, NumberInput, Select, DateInput
from datetime import datetime
from django.forms import forms
from core.ventas.models import Client, Sale

class ClienteForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    class Meta:
        model = Client
        fields = '__all__'
        exclude = ['balance']
        widgets = {
            'name' : TextInput(
                attrs= {
                    'placeholder': 'Nombres'
                }
            ),
            'surnames' : TextInput(
                attrs= {
                    'placeholder' : 'Apellidos',
                }
            ),
            'nit' : TextInput(
                attrs={
                    'placeholder' : 'NIT'
                }
            ),
            'address' : TextInput(
                attrs={
                    'placeholder' : 'Dirección'
                }
            ),
            'phone' : TextInput(
                attrs={
                    'placeholder' : 'Teléfono'
                }
            ),
            'term' : Select(
                attrs={
                    'placeholder' : 'Termino de Pago'
                }
            )
        }

class VentaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cli'].queryset = Client.objects.none()

    class Meta:
        model = Sale
        fields = '__all__'
        exclude= ['balance']
        widgets = {
            'cli': Select(attrs={
                'class': 'custom-select select2',
                # 'style': 'width: 100%'
            }),
            'term': Select(attrs={
               'class' : 'form-control'
            }),
            'date_sale': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'date_joined',
                    'data-target': '#date_joined',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'iva': TextInput(attrs={
                'class': 'form-control',
            }),
            'subtotal': TextInput(attrs={
                'class': 'form-control',
            }),
            'total': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            })
        }

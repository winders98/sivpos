from django.urls import path
from core.ventas.views import ClienteListView, ClienteUpdateView, ClieteCreateView, ClienteDeleteView, \
    VentaListView, VentaCreateView,VentaUpdateView, VentaDeleteView, VentaFacturaPdfView, CuentaPorPagar

app_name = 'venta'
urlpatterns = [
    path('cliente-list/', ClienteListView.as_view(), name="cliente-list"),
    path('cliente-create/', ClieteCreateView.as_view(), name="cliente-create"),
    path('cliente-update/<pk>/', ClienteUpdateView.as_view(), name="cliente-update"),
    path('cliente-delete/<pk>/', ClienteDeleteView.as_view(), name="cliente-delete"),
    path('venta-list/', VentaListView.as_view(), name ="venta-list"),
    path('venta-create/', VentaCreateView.as_view(), name="venta-create"),
    path('venta-update/<pk>/', VentaUpdateView.as_view(), name="venta-update"),
    path('venta-delete/<pk>/', VentaDeleteView.as_view(), name="venta-delete"),
    path('factura/<int:pk>/', VentaFacturaPdfView.as_view(), name='venta-factura'),
    path('cuentas/', CuentaPorPagar.as_view(), name="cuentas"),

]
import os
import json
from django.conf import settings
from django.db import transaction
from django.db.models import Q, Sum
from django.http import JsonResponse, HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, DeleteView, CreateView, View
from core.ventas.models import Client, Sale, DetSale, PaymentTerm
from core.erp.models import *
from core.ventas.forms import ClienteForm, VentaForm
from weasyprint import HTML, CSS


# Create your views here.

# Clientes-----------------------------------------------
class ClienteListView(ListView):
    model = Client
    template_name = 'cliente/list.html'
    context_object_name = 'clientes'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Client.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            # print (e)
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Lista de Clientes'
        context['create_url'] = reverse_lazy('venta:cliente-create')
        context['list_url'] = reverse_lazy('venta:cliente-list')
        context['entity'] = 'Clientes'
        return context

    def get_queryset(self):
        queryset = Client.objects.all()
        return queryset


class ClieteCreateView(CreateView):
    model = Client
    form_class = ClienteForm
    template_name = 'cliente/create.html'
    success_url = reverse_lazy('venta:cliente-list')

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Creacion de un Cliente"
        context['entity'] = 'Clientes'
        context['list_url'] = reverse_lazy('venta:cliente-list')
        context['acction'] = 'add'
        return context


class ClienteUpdateView(UpdateView):
    model = Client
    template_name = 'cliente/update.html'
    form_class = ClienteForm
    success_url = reverse_lazy('venta:cliente-list')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            acction = request.POST['acction']
            if acction == 'update':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción!'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Actualizar los datos del cliente"
        context['entity'] = 'Clientes'
        context['list_url'] = reverse_lazy('venta:cliente-list')
        context['acction'] = 'update'
        return context


class ClienteDeleteView(DeleteView):
    model = Client
    template_name = 'cliente/delete.html'
    success_url = reverse_lazy('venta:cliente-list')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Eliminar el cliente"
        context['entity'] = 'Clientes'
        context['list_url'] = reverse_lazy('venta:cliente-list')
        return context


# Ventas----------------------------------------------------------------------------------------------
class VentaListView(ListView):
    model = Sale
    template_name = 'venta/list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Sale.objects.filter(term__code='EFE').all()[0:15]:
                    data.append(i.toJSON())
            elif action == 'search_details_prod':
                data = []
                for i in DetSale.objects.filter(sale_id=request.POST['id']):
                    data.append(i.toJSON())
            elif action == 'chartdata':
                #data = []
                #labels = []
                data2=[]
                labels2 = []
                vents = Sale.objects.values('cli__names').annotate(Sum('total'))
                vents2 = Sale.objects.filter(
                    Q(total__gte=10000) | Q(total__gte=5000, total__lte=10000)
                    | Q(total__gte=1000, total__lte=5000) | Q(total__gte=0, total__lte=1000)
                ).count()
                print(vents2)
                # for i in Sale.objects.order_by('-total')[:5]:
                #     labels.append(i.cli.names)
                #     data.append(i.total)
                # return JsonResponse(data={
                #     'labels': labels,
                #     'data': data
                # })
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Lista de Ventas'
        context['create_url'] = reverse_lazy('venta:venta-create')
        context['list_url'] = reverse_lazy('venta:venta-list')
        context['entity'] = 'Ventas'
        return context


class VentaCreateView(CreateView):
    model = Sale
    template_name = 'venta/create.html'
    form_class = VentaForm
    success_url = reverse_lazy('venta:venta-list')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        termino = PaymentTerm.objects.filter(code__icontains='CRED').first()
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term'].strip()
                products = Product.objects.filter(stock__gt=0)
                if len(term):
                    products = products.filter(name__icontains=term)
                for i in products.exclude(id__in=ids_exclude)[0:10]:
                    item = i.toJSON()
                    item['value'] = i.name
                    # item['text'] = i.name
                    data.append(item)
            elif action == 'search_autocomplete':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term'].strip()
                data.append({'id': term, 'text': term})
                products = Product.objects.filter(name__icontains=term, stock__gt=0)
                for i in products.exclude(id__in=ids_exclude)[0:10]:
                    item = i.toJSON()
                    item['text'] = i.name
                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])
                    sale = Sale()
                    sale.date_sale = vents['date_sale']
                    sale.cli_id = vents['cli']
                    sale.term_id = vents['term_pago']
                    sale.subtotal = float(vents['subtotal'])
                    sale.iva = float(vents['iva'])
                    sale.total = float(vents['total'])
                    sale.term_id = vents['term_pago']
                    if sale.term == termino:
                        sale.balance = sale.total
                    else:
                        sale.balance = 0
                    sale.save()
                    for i in vents['products']:
                        det = DetSale()
                        det.sale_id = sale.id
                        det.prod_id = i['id']
                        det.cant = int(i['cant'])
                        det.price = float(i['pvp'])
                        det.subtotal = float(i['subtotal'])
                        det.save()
                        det.prod.stock -= det.cant
                        det.prod.save()
                    data = {'id': sale.id}
            elif action == 'search_clients':
                data = []
                term = request.POST['term']
                clients = Client.objects.filter(
                    Q(names__icontains=term) | Q(surnames__icontains=term) | Q(nit__icontains=term))[0:10]
                for i in clients:
                    item = i.toJSON()
                    item['text'] = i.get_full_name()
                    data.append(item)
            elif action == 'create_client':
                with transaction.atomic():
                    frmClient = ClienteForm(request.POST)
                    data = frmClient.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['det'] = []
        context['frmClient'] = ClienteForm()
        return context


class VentaUpdateView(UpdateView):
    model = Sale
    form_class = VentaForm
    template_name = 'venta/create.html'
    success_url = reverse_lazy('venta:venta-list')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        instance = self.get_object()
        form = VentaForm(instance=instance)
        form.fields['cli'].queryset = Client.objects.filter(id=instance.cli.id)
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term'].strip()
                products = Product.objects.filter(stock__gt=0)
                if len(term):
                    products = products.filter(name__icontains=term)
                for i in products.exclude(id__in=ids_exclude)[0:10]:
                    item = i.toJSON()
                    item['value'] = i.name
                    # item['text'] = i.name
                    data.append(item)
            elif action == 'search_autocomplete':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term'].strip()
                data.append({'id': term, 'text': term})
                products = Product.objects.filter(name__icontains=term, stock__gt=0)
                for i in products.exclude(id__in=ids_exclude)[0:10]:
                    item = i.toJSON()
                    item['text'] = i.name
                    data.append(item)
            elif action == 'edit':
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])
                    # sale = Sale.objects.get(pk=self.get_object().id)
                    sale = self.get_object()
                    sale.date_sale = vents['date_sale']
                    sale.cli_id = vents['cli']
                    sale.term_id = vents['term_pago']
                    sale.subtotal = float(vents['subtotal'])
                    sale.iva = float(vents['iva'])
                    sale.total = float(vents['total'])
                    sale.save()
                    sale.detsale_set.all().delete()
                    for i in vents['products']:
                        det = DetSale()
                        det.sale_id = sale.id
                        det.prod_id = i['id']
                        det.cant = int(i['cant'])
                        det.price = float(i['pvp'])
                        det.subtotal = float(i['subtotal'])
                        det.save()
                        det.prod.stock -= det.cant
                        det.prod.save()
                    data = {'id': sale.id}
            elif action == 'search_clients':
                data = []
                term = request.POST['term']
                clients = Client.objects.filter(
                    Q(names__icontains=term) | Q(surnames__icontains=term) | Q(dni__icontains=term))[0:10]
                for i in clients:
                    item = i.toJSON()
                    item['text'] = i.get_full_name()
                    data.append(item)
            elif action == 'create_client':
                with transaction.atomic():
                    frmClient = ClienteForm(request.POST)
                    data = frmClient.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_details_product(self):
        data = []
        try:
            for i in DetSale.objects.filter(sale_id=self.get_object().id):
                item = i.prod.toJSON()
                item['cant'] = i.cant
                data.append(item)
        except:
            pass
        return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        context['det'] = json.dumps(self.get_details_product())
        context['frmClient'] = ClienteForm()
        return context


class VentaDeleteView(DeleteView):
    model = Sale
    template_name = 'venta/delete.html'
    success_url = reverse_lazy('venta:venta-list')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        return context


class VentaFacturaPdfView(View):
    def get(self, request, *args, **kwargs):
        try:
            template = get_template('venta/invoice.html')
            context = {
                'sale': Sale.objects.get(pk=self.kwargs['pk']),
                'comp': {'name': 'Farmacia Segura S.A.', 'NIT': '9999999-9', 'address': 'San Francisco, Petén'},
                'icon': '{}{}'.format(settings.MEDIA_URL, 'logo.png')
            }
            html = template.render(context)
            css_url = os.path.join(settings.BASE_DIR,
                                   'static/lib/adminLTE3/plugins/bootstrap-4.4.1-dist/css/bootstrap.min.css')
            pdf = HTML(string=html, base_url=request.build_absolute_uri()).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('venta:venta-list'))


class CuentaPorPagar(ListView):
    model = Sale
    template_name = 'venta/accounts.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        termino = PaymentTerm.objects.filter(code__icontains='CRED').first()
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Sale.objects.filter(term=termino)[0:15]:
                    data.append(i.toJSON())
            elif action == 'createpay':
                #print(request.POST)
                account = json.loads(request.POST['sale'])
                monto = float(request.POST['monto'])
                if account:
                    sale = Sale.objects.filter(pk=account['id']).first()
                    if sale.balance > 0:
                        sale.balance = float(sale.balance) - monto
                        sale.save()
                pass
                #print(request.POST['sale'])
            # elif action == 'search_details_prod':
            #     data = []
            #     for i in DetSale.objects.filter(sale_id=request.POST['id']):
            #         data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Lista de Ventas Por Pagar'
        context['create_url'] = reverse_lazy('venta:venta-create')
        context['list_url'] = reverse_lazy('venta:venta-list')
        context['entity'] = 'Ventas por Pagar'
        context['action'] = 'createpay'
        return context

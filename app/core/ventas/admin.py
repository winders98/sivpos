from django.contrib import admin
from core.ventas.models import Client, PaymentTerm

# Register your models here.
admin.site.register(Client)
admin.site.register(PaymentTerm)

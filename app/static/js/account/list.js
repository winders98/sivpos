var tblSale;
var saleactual;

function format(d) {
    console.log(d);
    var html = '<table class="table">';
    html += '<thead class="thead-dark">';
    html += '<tr><th scope="col">Producto</th>';
    html += '<th scope="col">Categoría</th>';
    html += '<th scope="col">PVP</th>';
    html += '<th scope="col">Cantidad</th>';
    html += '<th scope="col">Subtotal</th></tr>';
    html += '</thead>';
    html += '<tbody>';
    $.each(d.det, function (key, value) {
        html += '<tr>'
        html += '<td>' + value.prod.name + '</td>'
        html += '<td>' + value.prod.cat.name + '</td>'
        html += '<td>' + value.price + '</td>'
        html += '<td>' + value.cant + '</td>'
        html += '<td>' + value.subtotal + '</td>'
        html += '</tr>';
    });
    html += '</tbody>';
    return html;
}

$(function () {

    tblSale = $('#data').DataTable({
        //responsive: true,
        scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "cli.names"},
            {"data": "date_sale"},
            {"data": "balance"},
            {"data": "total"},
            {"data": "id"},
        ],
        columnDefs: [
            {
                targets: [-2, -3,],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return 'Q' + parseFloat(data).toFixed(2);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a rel="details" class="btn btn-success btn-xs btn-flat"><i class="fas fa-cash-register"></i></a> ';

                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });


    $('#data tbody').on('click', 'a[rel="details"]', function () {
        var tr = tblSale.cell($(this).closest('td, li')).index();
        var data = tblSale.row(tr.row).data();
        var blance;
        saleactual = tblSale.row(tr.row).data();
        balance =parseFloat(data.balance);
        if(balance > 0){
            $('#myModalPay').modal('show');
        }


    })
        .on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tblSale.row(tr);
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

    $('#frmPay').on('submit', function (e) {
        e.preventDefault();

        var acction = $('input[name="action"]').val();
        var monto = $('input[name="monto"]').val()
        var sale = ('sale', JSON.stringify(saleactual));
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': acction,
                'monto': monto,
                'sale': sale
            },
            dataType: 'json',
        })
            .done(function (data) {
                if (!data.hasOwnProperty('error')) {
                    $('#myModalPay').modal('hide');
                    callback();
                    return false;
                }
                //console.log(data);
            })
            .fail(function (data) {
                alert("Error");
            })
            .always(function (data) {
            })
    });


    function callback() {
        location.href = '/cuentas/';
    }

});